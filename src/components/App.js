import React from "react";
import DisplayStats from "./DisplayStats";
import GameLog from "./GameLog";
import OfficalPage from "./OfficalPage";
import StartPage from "./StartPage";
import { HashRouter, Route } from "react-router-dom";
import Header from "./Header";

const App = () => {
  return (
    <div>
      <HashRouter>
        <div>
          <Header />
          <br />
          <Route path="/" exact component={DisplayStats} />
          <Route path="/log" exact component={GameLog} />
          <Route path="/offical" exact component={OfficalPage} />
          <Route path="/start" exact component={StartPage} />
        </div>
      </HashRouter>
    </div>
  );
};

export default App;
