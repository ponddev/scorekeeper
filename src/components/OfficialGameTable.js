import React from "react";
import { useSelector } from "react-redux";
import OfficialEntry from "./OfficialEntry";

const OfficialGameTable = (props) => {
  let startIndex = props.index;
  let max = props.index + props.max;
  const allEntries = useSelector((state) => state.logEntries);
  let array = [];
  for (let i = startIndex; i < allEntries.length && i < max; i++) {
    array.splice(i, 0, allEntries[i]);
  }

  const renderTable = () => {
    if (startIndex >= allEntries.length) {
      return;
    } else {
      return (
        <table className="ui celled table">
          <tr>
            <th>Time</th>
            <th>W</th>
            <th>D</th>
            <th>Remarks</th>
            <th>W-D</th>
          </tr>
          {array.map((entry, index) => renderEntry(entry, index))}
        </table>
      );
    }
  };

  const renderEntry = (entry, index) => {
    return (
      <OfficialEntry
        time={`${entry.minute}:${String(entry.second).padStart(2, "0")}`}
        cap={entry.cap}
        remark={entry.remark}
        score={props.score[index + startIndex]}
        isWhite={entry.isWhite}
      />
    );
  };
  return <div>{renderTable()}</div>;
};

export default OfficialGameTable;
