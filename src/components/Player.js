import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import ReactModal from "react-modal";
import "./styles.css";
import ModalForm from "./ModalForm";

const Player = (props) => {
  const [modalOpen, setModalOpen] = useState(false);
  const [playerAction, setPlayerAction] = useState("");

  const allEntries = useSelector((state) => state.logEntries);

  const getInfoForPlayer = (capNum, isWhite) => {
    let playerInfo = { numKO: 0, numP: 0, numG: 0 };

    for (let i = 0; i < allEntries.length; i++) {
      if (
        allEntries[i].cap === capNum &&
        allEntries[i].isWhite === props.isWhite
      ) {
        if (allEntries[i].remark === "E") {
          playerInfo.numKO++;
        }
        if (allEntries[i].remark === "P") {
          playerInfo.numP++;
        }
        if (allEntries[i].remark === "G") {
          playerInfo.numG++;
        }
        let totalFouls = playerInfo.numP + playerInfo.numKO;
        if (totalFouls === 2) {
          playerInfo.color = "yellow";
        } else if (totalFouls === 3) {
          playerInfo.color = "red";
        }
      }
    }

    return playerInfo;
  };

  const [cNum, setCNum] = useState(props.cap);
  const dispatch = useDispatch();
  const isWhite = props.isWhite;
  const closeModal = () => {
    setModalOpen(false);
  };

  const onK = () => {
    setPlayerAction("kickout");
    setModalOpen(true);
  };

  const onP = () => {
    setPlayerAction("penalty");
    setModalOpen(true);
  };

  const onClick = (event) => {
    setPlayerAction("goal");
    setModalOpen(true);
  };

  const playerInfo = getInfoForPlayer(props.cap, props.isWhite);

  return (
    <div className="item">
      <div className="ui huge label">{props.cap}</div>
      <div className="ui labeled button" tabIndex="0">
        <div className={`ui ${playerInfo.color} button`} onClick={onK}>
          K
        </div>
        <a className={`ui basic ${playerInfo.color} left pointing label`}>
          {playerInfo.numKO}
        </a>
      </div>
      <ReactModal
        isOpen={modalOpen}
        className="mymodal"
        overlayClassName="myoverlay"
      >
        <ModalForm
          action={playerAction}
          cap={cNum}
          isWhite={isWhite}
          team={props.team}
          playerInfo={playerInfo}
          closeModal={closeModal}
        />
      </ReactModal>
      <div className="ui labeled button" tabIndex="0">
        <div className={`ui ${playerInfo.color} button`} onClick={onP}>
          P
        </div>
        <a className={`ui basic ${playerInfo.color} left pointing label`}>
          {playerInfo.numP}
        </a>
      </div>
      <ReactModal
        isOpen={modalOpen}
        className="mymodal"
        overlayClassName="myoverlay"
      >
        <ModalForm
          action={playerAction}
          cap={cNum}
          isWhite={isWhite}
          team={props.team}
          playerInfo={playerInfo}
          closeModal={closeModal}
        />
      </ReactModal>
      <div className="ui labeled button" tabIndex="0">
        <div class="ui  button" onClick={onClick}>
          G
        </div>
        <a className="ui basic  left pointing label">{playerInfo.numG}</a>
      </div>
      <ReactModal
        isOpen={modalOpen}
        className="mymodal"
        overlayClassName="myoverlay"
      >
        <ModalForm
          action={playerAction}
          cap={cNum}
          isWhite={isWhite}
          team={props.team}
          playerInfo={playerInfo}
          onGoal={props.onGoal}
          closeModal={closeModal}
        />
      </ReactModal>
    </div>
  );
};

export default Player;
