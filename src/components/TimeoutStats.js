import react from "react";
import { useSelector } from "react-redux";

const TimeoutStats = (props) => {
  const allEntries = useSelector((state) => state.logEntries);

  let timeOutOne = "";
  let timeOutTwo = "";
  let timeOutThree = "";
  let timeOutFour = "";

  for (let i = 0; i < allEntries.length; i++) {
    if (
      allEntries[i].remark === "TO" &&
      allEntries[i].isWhite === props.isWhite
    ) {
      if (timeOutOne === "") {
        timeOutOne = `${allEntries[i].period} / ${
          allEntries[i].minute
        }:${String(allEntries[i].second).padStart(2, "0")}`;
      } else if (timeOutTwo === "") {
        timeOutTwo = `${allEntries[i].period} / ${
          allEntries[i].minute
        }:${String(allEntries[i].second).padStart(2, "0")}`;
      } else if (timeOutThree === "") {
        timeOutThree = `${allEntries[i].period} / ${
          allEntries[i].minute
        }:${String(allEntries[i].second).padStart(2, "0")}`;
      } else if (timeOutFour === "") {
        timeOutFour = `${allEntries[i].period} / ${
          allEntries[i].minute
        }:${String(allEntries[i].second).padStart(2, "0")}`;
      }
    }
  }

  return (
    <div className="ui two column grid">
      <div className="column">
        <h2>Timeouts</h2>
      </div>
      <div className="column">
        <table className="ui celled table">
          <tr>
            <td>{timeOutOne}</td>
            <td>{timeOutTwo}</td>
            <td>{timeOutThree}</td>
            <td>{timeOutFour}</td>
          </tr>
        </table>
      </div>
    </div>
  );
};

export default TimeoutStats;
