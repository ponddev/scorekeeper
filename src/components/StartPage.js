import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import ReactModal from "react-modal";
import "./styles.css";

const StartPage = () => {
  const [darkTeam, setDarkTeam] = useState("");
  const [whiteTeam, setWhiteTeam] = useState("");
  const [timeoutNum, setTimeoutNum] = useState("");
  const [otNum, setOtNum] = useState("");
  const [periodLength, setPeriodLength] = useState("");
  const [playerNum, setPlayerNum] = useState("");
  const [closeModal, setCloseModal] = useState(false);

  const game = useSelector((state) => state.setUp);

  const dispatch = useDispatch();

  const onDark = (event) => {
    setDarkTeam(event.target.value);
  };

  const onWhite = (event) => {
    setWhiteTeam(event.target.value);
  };

  const onTimeout = (event) => {
    setTimeoutNum(event.target.value);
  };

  const onOt = (event) => {
    setOtNum(event.target.value);
  };

  const onLength = (event) => {
    setPeriodLength(event.target.value);
  };

  const onNum = (event) => {
    setPlayerNum(event.target.value);
  };

  const onGame = () => {
    setCloseModal(true);
  };

  const closeYesModal = () => {
    dispatch({
      type: "CREATE_GAME",
      payload: {
        white: whiteTeam,
        dark: darkTeam,
        numTo: timeoutNum,
        numOt: otNum,
        lengthPeriod: periodLength,
        numPlayers: playerNum,
      },
    });
    setCloseModal(false);
  };

  const closeNoModal = () => {
    setCloseModal(false);
  };

  return (
    <div>
      <h1>Start Page</h1>
      <form className="ui form">
        <div className="field">
          <label>
            White Team Name:
            <input type="text" value={whiteTeam} onChange={onWhite} />
          </label>
        </div>
        <div className="field">
          <label>
            Dark Team Name:
            <input type="text" value={darkTeam} onChange={onDark} />
          </label>
        </div>
        <div className="field">
          <label>
            Number TO
            <input type="text" value={timeoutNum} onChange={onTimeout} />
          </label>
        </div>
        <div className="field">
          <label>
            Number of OT
            <input type="text" value={otNum} onChange={onOt} />
          </label>
        </div>
        <div className="field">
          <label>
            Length of period
            <input type="text" value={periodLength} onChange={onLength} />
          </label>
        </div>
        <div className="field">
          <label>
            Number of players
            <input type="text" value={playerNum} onChange={onNum} />
          </label>
        </div>
      </form>
      <button className="ui button" onClick={onGame}>
        Create New Game
      </button>
      <ReactModal
        isOpen={closeModal}
        className="mymodal"
        overlayClassName="myoverlay"
      >
        <p> Are sure you want to create a new game?</p>
        <button onClick={closeYesModal}> Yes</button>
        <button onClick={closeNoModal}>No</button>
      </ReactModal>
    </div>
  );
};

export default StartPage;
