import React, { useState } from "react";
import { useDispatch } from "react-redux";
import ReactModal from "react-modal";
import "./styles.css";

const LogEntry = (props) => {
  const [closeModal, setCloseModal] = useState(false);
  const dispatch = useDispatch();
  const remarkG = props.remark === "G";
  const getScore = () => {
    if (props.remark === "G") {
      return (
        <td data-label="White-Dark">{`${props.score.white}- ${props.score.dark}`}</td>
      );
    } else {
      return <td data-label="White-Dark"></td>;
    }
  };

  const getAlignment = () => {
    if (props.remark === "G") {
      return "left aligned";
    }

    if (props.remark === "TO") {
      return "center aligned";
    }

    if (props.remark === "RC") {
      return "center aligned";
    }

    if (props.remark === "YC") {
      return "center aligned";
    }
    return "right aligned";
  };
  const click = () => {
    setCloseModal(true);
  };
  const closeYesModal = () => {
    dispatch({
      type: "DELETE",
      payload: props.index,
    });
    setCloseModal(false);
  };
  const closeNoModal = () => {
    setCloseModal(false);
  };
  const periodChangeClass = props.periodchange ? "periodchange" : "";
  return (
    <tr className={periodChangeClass}>
      <td data-label="time">
        {props.time}
        <button
          className="ui icon button"
          title="Delete game log entry"
          onClick={click}
        >
          <i className="trash icon"></i>
        </button>{" "}
        <ReactModal
          isOpen={closeModal}
          className="mymodal"
          overlayClassName="myoverlay"
        >
          <p> Are sure you want to delete entry from GameLog?</p>
          <button onClick={closeYesModal}> Yes</button>
          <button onClick={closeNoModal}>No</button>
        </ReactModal>
        <button className="ui icon button" title="Edit game log entry">
          <i className="edit icon"></i>
        </button>
      </td>
      <td data-label="cap">{props.isWhite && props.cap}</td>
      <td data-label="cap" className="active">
        {!props.isWhite && props.cap}
      </td>
      <td data-label="remark" className={`${getAlignment()}`}>
        {props.remark}
      </td>
      {getScore()}
    </tr>
  );
};

export default LogEntry;
