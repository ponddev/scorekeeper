import React from "react";

const OfficalEntry = (props) => {
  const getScore = () => {
    if (props.remark === "G") {
      return <td>{props.score}</td>;
    } else {
      return <td></td>;
    }
  };
  const getAlignment = () => {
    if (props.remark === "G") {
      return "left aligned";
    }

    if (props.remark === "TO") {
      return "center aligned";
    }

    if (props.remark === "RC") {
      return "center aligned";
    }

    if (props.remark === "YC") {
      return "center aligned";
    }
    return "right aligned";
  };
  return (
    <tr>
      <td>{props.time}</td>
      <td>{props.isWhite && props.cap}</td>
      <td>{!props.isWhite && props.cap}</td>
      <td className={`${getAlignment()}`}>{props.remark}</td>
      {getScore()}
    </tr>
  );
};

export default OfficalEntry;
