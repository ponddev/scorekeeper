import React from "react";
import { Link } from "react-router-dom";

const Header = () => {
  return (
    <div className="ui secondary pointing menu headermenu">
      <Link to="/" className="item">
        Display
      </Link>
      <div className="right menu">
        <Link to="/log" className="item">
          Game Log
        </Link>
        <Link to="/offical" className="item">
          Offical Page
        </Link>
        <Link to="/start" className="item">
          Start
        </Link>
      </div>
    </div>
  );
};

export default Header;
