import react from "react";

const QuaterLogEntry = (props) => {
  const getScore = () => {
    if (props.remark === "G") {
      return (
        <td data-label="White-Dark">{`${props.score.white}- ${props.score.dark}`}</td>
      );
    } else {
      return <td data-label="White-Dark"></td>;
    }
  };

  const getAlignment = () => {
    if (props.remark === "G") {
      return "left aligned";
    }

    if (props.remark === "TO") {
      return "center aligned";
    }

    if (props.remark === "RC") {
      return "center aligned";
    }

    if (props.remark === "YC") {
      return "center aligned";
    }
    return "right aligned";
  };
  return (
    <tr>
      <td>{props.time}</td>
      <td>{props.isWhite && props.cap}</td>
      <td>{!props.isWhite && props.cap}</td>
      <td className={`${getAlignment()}`}>{props.remark}</td>
      <td>{getScore()}</td>
    </tr>
  );
};

export default QuaterLogEntry;
