import React, { useState } from "react";
import Score from "./Score";
import Player from "./Player";
import { useDispatch, useSelector } from "react-redux";
import ReactModal from "react-modal";
import "./styles.css";
import ModalForm from "./ModalForm";

const TeamStats = (props) => {
  const allEntries = useSelector((state) => state.logEntries);

  const dispatch = useDispatch();

  let array = [];

  const currentPeriod = useSelector((state) => state.period);

  const [playerAction, setPlayerAction] = useState("");

  const [modalOpen, setModalOpen] = useState(false);

  const closeModal = () => {
    setModalOpen(false);
  };

  const getInfo = () => {
    let info = { numTO: 0 };

    for (let i = 0; i < allEntries.length; i++) {
      if (
        allEntries[i].remark === "TO" &&
        allEntries[i].isWhite === props.isWhite
      ) {
        info.numTO++;
      }
    }
    return info;
  };

  // const [PlayerList, setPlayerList] = useState([]);

  // const [capNumber, setCapNumber] = useState(1);
  const game = useSelector((state) => state.setUp);

  const numPlayers = Number(game.numPlayers);

  const getPlayerList = () => {
    let playerList = [];
    for (let i = 1; i <= numPlayers; i++) {
      playerList.push({ cap: `${i}` });
      if (i === 1) {
        playerList.push({ cap: "1a" });
      }
    }
    return playerList;
  };

  let playerList = getPlayerList();

  //const onAddPlayer = () => {
  //  let sCapNum = `${capNumber + 1}`;
  //  setCapNumber(capNumber + 1);
  //  dispatch({
  //   type: "ADD_PLAYER",
  //   payload: {
  //     cap: sCapNum,
  //     isWhite: props.isWhite,
  //   },
  //  });
  // setPlayerList([...PlayerList, { cap: sCapNum }]);
  // };

  const onT = () => {
    setPlayerAction("Timeout");
    setModalOpen(true);
  };

  const onR = () => {
    setPlayerAction("Redcard");
    setModalOpen(true);
  };

  const onY = () => {
    setPlayerAction("Yellowcard");
    setModalOpen(true);
  };

  const [count, setCount] = useState(0);
  const onGoal = (isWhite) => {
    let scoreInfo = { score: 0 };
    for (let i = 0; i < allEntries.length; i++) {
      if (
        allEntries[i].isWhite === props.isWhite &&
        allEntries[i].remark === "G"
      ) {
        scoreInfo.score++;
      }
    }
    return scoreInfo;
  };

  const scoreInfo = onGoal(props.isWhite);

  const info = getInfo();

  const genarate = (entry) => {
    dispatch({
      type: "ADD_ENTRY",
      payload: {
        period: entry.period,
        minute: Number(entry.minute),
        second: Number(`${entry.secondTen}${entry.secondOne}`),
        cap: entry.cap,
        remark: entry.remark,
        isWhite: props.isWhite,
      },
    });
  };

  const onFake = () => {
    let newArray = array;
    for (let i = 0; i < 65; i++) {
      newArray.push({
        period: "1",
        minute: "8",
        secondTen: 0,
        secondOne: 0,
        cap: "1",
        remark: "G",
        isWhite: props.isWhite,
      });
    }
    dispatch({
      type: "ADD_ENTRIES",
      payload: {
        array: newArray,
      },
    });
  };

  return (
    <div className="ui container">
      <Score totalscore={scoreInfo.score} />
      <div className="ui divider"></div>
      <div className="ui items">
        <div className="items">
          <div className="ui huge label">{props.team}</div>
          <div className="ui large label">
            Period
            <div className="detail">{currentPeriod}</div>
          </div>
        </div>
        <div className="item">
          <div className="ui labeled button" tabIndex="0">
            <div className={`ui  button`} onClick={onT}>
              TO
            </div>
            <a className={`ui basic  left pointing label`}>{info.numTO}</a>
          </div>
          <ReactModal
            isOpen={modalOpen}
            className="mymodal"
            overlayClassName="myoverlay"
          >
            <ModalForm
              action={playerAction}
              isWhite={props.isWhite}
              team={props.team}
              cap={"C"}
              playerInfo={info}
              closeModal={closeModal}
            />
          </ReactModal>
          <div className="ui button" onClick={onR}>
            {" "}
            R
          </div>
          <ReactModal
            isOpen={modalOpen}
            className="mymodal"
            overlayClassName="myoverlay"
          >
            <ModalForm
              action={playerAction}
              isWhite={props.isWhite}
              team={props.team}
              cap={"C"}
              playerInfo={info}
              closeModal={closeModal}
            />
          </ReactModal>
          <div className="ui button" onClick={onY}>
            {" "}
            Y
          </div>

          <ReactModal
            isOpen={modalOpen}
            className="mymodal"
            overlayClassName="myoverlay"
          >
            <ModalForm
              action={playerAction}
              isWhite={props.isWhite}
              team={props.team}
              cap={"C"}
              playerInfo={info}
              closeModal={closeModal}
            />
          </ReactModal>
          <div className="ui button" onClick={onFake}>
            Fake
          </div>
        </div>

        {playerList.map((player, i) => (
          <div className="item">
            <Player
              cap={player.cap}
              onGoal={onGoal}
              team={props.team}
              isWhite={props.isWhite}
              key={player.capNumber}
            />
            <br />
          </div>
        ))}
      </div>
      <div className="ui row">
        <button className="ui button">Add Player</button>
      </div>
    </div>
  );
};

export default TeamStats;
