import React from "react";
import OfficialGameTable from "./OfficialGameTable";
import { useSelector } from "react-redux";
import Results from "./Results";

const OfficialGameLog = () => {
  const allEntries = useSelector((state) => state.logEntries);
  let score = { white: 0, dark: 0 };
  let scoreArray = [];
  const calcScore = (score, entry) => {
    let newScore = { ...score };

    if (entry.remark === "G") {
      if (entry.isWhite) {
        newScore.white++;
      } else {
        newScore.dark++;
      }
    }
    return newScore;
  };
  for (let i = 0; i < allEntries.length; i++) {
    score = calcScore(score, allEntries[i]);
    scoreArray.splice(i, 0, `${score.white}-${score.dark}`);
  }
  return (
    <div className=" ui five column grid">
      <div className="column">
        <OfficialGameTable index={0} max={15} score={scoreArray} />
      </div>
      <div className="column">
        <OfficialGameTable index={15} max={15} score={scoreArray} />
      </div>
      <div className="column">
        <OfficialGameTable index={30} max={15} score={scoreArray} />
      </div>
      <div className="column">
        <OfficialGameTable index={45} max={15} score={scoreArray} />
      </div>
      <div className="column">
        <OfficialGameTable index={60} max={5} score={scoreArray} />
        <br />
        <Results />
      </div>
    </div>
  );
};

export default OfficialGameLog;
