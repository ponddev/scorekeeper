import React, { useState } from "react";
import { useSelector } from "react-redux";
import LogEntry from "./LogEntry";

const GameLog = () => {
  const allEntries = useSelector((state) => state.logEntries);

  let score = { white: 0, dark: 0 };
  let currentPeriod = "1";
  const calcScore = (score, entry) => {
    let newScore = { ...score };

    if (entry.remark === "G") {
      if (entry.isWhite) {
        newScore.white++;
      } else {
        newScore.dark++;
      }
    }
    return newScore;
  };

  const renderLogEntry = (entry, index) => {
    const periodChange = currentPeriod !== entry.period;

    currentPeriod = entry.period;

    return (
      <LogEntry
        time={`${entry.minute}:${String(entry.second).padStart(2, "0")}`}
        cap={entry.cap}
        remark={entry.remark}
        score={(score = calcScore(score, entry))}
        isWhite={entry.isWhite}
        index={index}
        periodchange={periodChange}
      />
    );
  };

  return (
    <div>
      <table class="ui celled table">
        <thead>
          <tr>
            <th rowspan="2">Time</th>
            <th colspan="2" class="center algined">
              Cap Number
            </th>
            <th rowspan="2">Remarks</th>
            <th rowspan="2"> White-Dark</th>
          </tr>
          <tr>
            <th>W</th>
            <th>D</th>
          </tr>
        </thead>
        <tbody>
          {allEntries.map((entry, index) => renderLogEntry(entry, index))}
        </tbody>
      </table>
    </div>
  );
};

export default GameLog;
