import react, { useState } from "react";
import { useSelector } from "react-redux";
import PlayerEntry from "./PlayerEntry";

const OfficalTeamStats = (props) => {
  const allEntries = useSelector((state) => state.logEntries);

  let color = "White";
  if (props.isWhite === false) {
    color = "Dark";
  }

  const getStatsPerPlayer = (cap, isWhite) => {
    let oneGoals = 0;
    let twoGoals = 0;
    let threeGoals = 0;
    let fourGoals = 0;
    let otGoals = 0;
    let otTwoGoals = 0;
    let foulOne = "";
    let foulTwo = "";
    let foulThree = "";

    for (let i = 0; i < allEntries.length; i++) {
      if (allEntries[i].cap === cap && allEntries[i].isWhite === isWhite) {
        if (allEntries[i].remark === "G") {
          if (allEntries[i].period === "1") {
            oneGoals++;
          } else if (allEntries[i].period === "2") {
            twoGoals++;
          }
          if (allEntries[i].period === "3") {
            threeGoals++;
          } else if (allEntries[i].period === "4") {
            fourGoals++;
          } else if (allEntries[i].period === "OT") {
            otGoals++;
          } else if (allEntries[i].period === "OT2") {
            otTwoGoals++;
          }
        }

        if (allEntries[i].remark === "E") {
          if (foulOne === "") {
            foulOne = `E${allEntries[i].period}`;
          } else if (foulOne !== "" && foulTwo === "") {
            foulTwo = `E${allEntries[i].period}`;
          } else if (foulOne !== "" && foulTwo !== "" && foulThree === "") {
            foulThree = `E${allEntries[i].period}`;
          }
        }
        if (allEntries[i].remark === "P") {
          if (foulOne === "") {
            foulOne = `P${allEntries[i].period}`;
          } else if (foulOne !== "" && foulTwo === "") {
            foulTwo = `P${allEntries[i].period}`;
          } else if (foulOne !== "" && foulTwo !== "" && foulThree === "") {
            foulThree = `P${allEntries[i].period}`;
          }
        }
      }
    }
    return (
      <PlayerEntry
        cap={cap}
        oneGoals={oneGoals}
        twoGoals={twoGoals}
        threeGoals={threeGoals}
        fourGoals={fourGoals}
        otGoals={otGoals}
        otTwoGoals={otTwoGoals}
        foulOne={foulOne}
        foulTwo={foulTwo}
        foulThree={foulThree}
      />
    );
  };
  return (
    <div>
      <h1>
        {`${props.team}`}
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{`(${color})`}
      </h1>
      <table className="ui celled table offical. ">
        <tr className="offical.ui.table th">
          <th>Cap#</th>
          <th>1st</th>
          <th>2nd</th>
          <th>3rd</th>
          <th>4th</th>
          <th>OT</th>
          <th>OT2</th>
          <th>Per Foul</th>
          <th>Per Foul</th>
          <th>Per Foul</th>
        </tr>
        {props.playerList.map((cap) =>
          getStatsPerPlayer(cap.cap, props.isWhite)
        )}
      </table>
    </div>
  );
};

export default OfficalTeamStats;
