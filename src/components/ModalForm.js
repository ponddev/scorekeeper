import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";

const ModalForm = (props) => {
  const currentTime = useSelector((state) => state.time);
  const [period, setPeriod] = useState("1");
  const [minute, setMinute] = useState(currentTime.minute);
  const [secondTen, setSecondTen] = useState(currentTime.tenSecond);
  const [secondOne, setSecondOne] = useState(currentTime.oneSecond);

  const dispatch = useDispatch();

  const currentPeriod = useSelector((state) => state.period);

  const handleMinute = (event) => {
    setMinute(event.target.value);
  };

  const handleSecondTen = (event) => {
    setSecondTen(event.target.value);
  };

  const handleSecondOne = (event) => {
    setSecondOne(event.target.value);
  };

  const handlePeriod = (event) => {
    setPeriod(event.target.value);
  };

  const getMessage = () => {
    return `Add ${props.action} to player ${props.cap} from ${props.team}`;
  };
  const onOK = () => {
    if (props.action === "kickout") {
      if (props.playerInfo.numKO + props.playerInfo.numP < 3) {
        dispatch({
          type: "ADD_ENTRY",
          payload: {
            period: period,
            minute: Number(minute),
            second: Number(`${secondTen}${secondOne}`),
            cap: props.cap,
            remark: "E",
            isWhite: props.isWhite,
          },
        });
        if (period > currentPeriod) {
          dispatch({
            type: "CHANGE_PERIOD",
            payload: {
              period: period,
            },
          });
        }
        if (period >= currentPeriod) {
          dispatch({
            type: "CHANGE_TIME",
            payload: {
              minute: minute,
              tenSecond: secondTen,
              oneSecond: secondOne,
            },
          });
        }
      }
    }
    if (props.action === "penalty") {
      if (props.playerInfo.numKO + props.playerInfo.numP < 3) {
        dispatch({
          type: "ADD_ENTRY",
          payload: {
            period: period,
            minute: Number(minute),
            second: Number(`${secondTen}${secondOne}`),
            cap: props.cap,
            remark: "P",
            isWhite: props.isWhite,
          },
        });
        if (period > currentPeriod) {
          dispatch({
            type: "CHANGE_PERIOD",
            payload: {
              period: period,
            },
          });
        }
        if (period >= currentPeriod) {
          dispatch({
            type: "CHANGE_TIME",
            payload: {
              minute: minute,
              tenSecond: secondTen,
              oneSecond: secondOne,
            },
          });
        }
      }
    }
    if (props.action === "goal") {
      props.onGoal(props.isWhite);
      dispatch({
        type: "ADD_ENTRY",
        payload: {
          period: period,
          minute: Number(minute),
          second: Number(`${secondTen}${secondOne}`),
          cap: props.cap,
          remark: "G",
          isWhite: props.isWhite,
        },
      });
      if (period > currentPeriod) {
        dispatch({
          type: "CHANGE_PERIOD",
          payload: {
            period: period,
          },
        });
      }
      if (period >= currentPeriod) {
        dispatch({
          type: "CHANGE_TIME",
          payload: {
            minute: minute,
            tenSecond: secondTen,
            oneSecond: secondOne,
          },
        });
      }
    }
    if (props.action === "Timeout") {
      dispatch({
        type: "ADD_ENTRY",
        payload: {
          period: period,
          minute: Number(minute),
          second: Number(`${secondTen}${secondOne}`),
          cap: props.cap,
          remark: "TO",
          isWhite: props.isWhite,
        },
      });
      if (period > currentPeriod) {
        dispatch({
          type: "CHANGE_PERIOD",
          payload: {
            period: period,
          },
        });
      }
      if (period >= currentPeriod) {
        dispatch({
          type: "CHANGE_TIME",
          payload: {
            minute: minute,
            tenSecond: secondTen,
            oneSecond: secondOne,
          },
        });
      }
    }
    if (props.action === "Redcard") {
      dispatch({
        type: "ADD_ENTRY",
        payload: {
          period: period,
          minute: Number(minute),
          second: Number(`${secondTen}${secondOne}`),
          cap: " ",
          remark: "RC",
          isWhite: props.isWhite,
        },
      });
      if (period > currentPeriod) {
        dispatch({
          type: "CHANGE_PERIOD",
          payload: {
            period: period,
          },
        });
      }
      if (period >= currentPeriod) {
        dispatch({
          type: "CHANGE_TIME",
          payload: {
            minute: minute,
            tenSecond: secondTen,
            oneSecond: secondOne,
          },
        });
      }
    }
    if (props.action === "Yellowcard") {
      dispatch({
        type: "ADD_ENTRY",
        payload: {
          period: period,
          minute: Number(minute),
          second: Number(`${secondTen}${secondOne}`),
          cap: " ",
          remark: "YC",
          isWhite: props.isWhite,
        },
      });
      if (period > currentPeriod) {
        dispatch({
          type: "CHANGE_PERIOD",
          payload: {
            period: period,
          },
        });
      }
      if (period >= currentPeriod) {
        dispatch({
          type: "CHANGE_TIME",
          payload: {
            minute: minute,
            tenSecond: secondTen,
            oneSecond: secondOne,
          },
        });
      }
    }
    props.closeModal();
  };

  const OnCancel = () => {
    props.closeModal();
  };
  return (
    <div>
      <h3>{getMessage()}</h3>
      <form className="ui form">
        <div className="flexrow">
          <span className="myminute">
            <select
              className="ui dropdown minute"
              onChange={handleMinute}
              value={minute}
            >
              <option value="8">8</option>
              <option value="7">7</option>
              <option value="6">6</option>
              <option value="5">5</option>
              <option value="4">4</option>
              <option value="3">3</option>
              <option value="2">2</option>
              <option value="1">1</option>
              <option value="0">0</option>
            </select>
          </span>

          <select
            className="ui dropdown second"
            value={secondTen}
            onChange={handleSecondTen}
          >
            <option value="5">5</option>
            <option value="4">4</option>
            <option value="3">3</option>
            <option value="2">2</option>
            <option value="1">1</option>
            <option value="0">0</option>
          </select>

          <select
            className="ui  dropdown second"
            value={secondOne}
            onChange={handleSecondOne}
          >
            <option value="9">9</option>
            <option value="8">8</option>
            <option value="7">7</option>
            <option value="6">6</option>
            <option value="5">5</option>
            <option value="4">4</option>
            <option value="3">3</option>
            <option value="2">2</option>
            <option value="1">1</option>
            <option value="0">0</option>
          </select>
        </div>
        <div className="flexRow">
          <select
            className="ui  dropdown period"
            value={period}
            onChange={handlePeriod}
          >
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="OT">OT</option>
            <option value="OT2">OT2</option>
          </select>
        </div>

        <button className="ui button" onClick={onOK}>
          OK
        </button>
        <button className="ui button" onClick={OnCancel}>
          Cancel
        </button>
      </form>
    </div>
  );
};

export default ModalForm;
