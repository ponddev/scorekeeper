import react from "react";

const getValue = (value) => {
  return (value > 0 ? value: '');
};

const PlayerEntry = (props) => {
  return (
    <tr className="offical.ui.table th">
      <td>{props.cap}</td>
      <td>{getValue(props.oneGoals)}</td>
      <td>{getValue(props.twoGoals)}</td>
      <td>{getValue(props.threeGoals)}</td>
      <td>{getValue(props.fourGoals)}</td>
      <td>{getValue(props.otGoals)}</td>
      <td>{getValue(props.otTwoGoals)}</td>
      <td>{getValue(props.foulOne)}</td>
      <td>{getValue(props.foulTwo)}</td>
      <td>{getValue(props.foulThree)}</td>
    </tr>
  );
};

export default PlayerEntry;
