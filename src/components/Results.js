import react from "react";
import { useSelector } from "react-redux";

const Results = () => {
  const allEntries = useSelector((state) => state.logEntries);

  let scoreOne = { white: 0, dark: 0 };
  let scoreTwo = { white: 0, dark: 0 };
  let scoreThree = { white: 0, dark: 0 };
  let scoreFour = { white: 0, dark: 0 };
  let scoreOt = { white: 0, dark: 0 };
  let scoreOtTwo = { white: 0, dark: 0 };

  const getFirstQuaterScore = (score) => {
    let newScore = { ...score };
    for (let i = 0; i < allEntries.length; i++) {
      if (allEntries[i].period === "1") {
        if (allEntries[i].remark === "G" && allEntries[i].isWhite === true) {
          newScore.white++;
        } else if (
          allEntries[i].remark === "G" &&
          allEntries[i].isWhite === false
        ) {
          newScore.dark++;
        }
      }
    }
    return newScore;
  };

  const getSecondQuaterScore = (score) => {
    let newScore = { ...score };
    for (let i = 0; i < allEntries.length; i++) {
      if (allEntries[i].period === "2") {
        if (allEntries[i].remark === "G" && allEntries[i].isWhite === true) {
          newScore.white++;
        } else if (
          allEntries[i].remark === "G" &&
          allEntries[i].isWhite === false
        ) {
          newScore.dark++;
        }
      }
    }
    return newScore;
  };

  const getThirdQuaterScore = (score) => {
    let newScore = { ...score };
    for (let i = 0; i < allEntries.length; i++) {
      if (allEntries[i].period === "3") {
        if (allEntries[i].remark === "G" && allEntries[i].isWhite === true) {
          newScore.white++;
        } else if (
          allEntries[i].remark === "G" &&
          allEntries[i].isWhite === false
        ) {
          newScore.dark++;
        }
      }
    }
    return newScore;
  };

  const getFourthQuaterScore = (score) => {
    let newScore = { ...score };
    for (let i = 0; i < allEntries.length; i++) {
      if (allEntries[i].period === "4") {
        if (allEntries[i].remark === "G" && allEntries[i].isWhite === true) {
          newScore.white++;
        } else if (
          allEntries[i].remark === "G" &&
          allEntries[i].isWhite === false
        ) {
          newScore.dark++;
        }
      }
    }
    return newScore;
  };

  const getOtQuaterScore = (score) => {
    let newScore = { ...score };
    for (let i = 0; i < allEntries.length; i++) {
      if (allEntries[i].period === "OT") {
        if (allEntries[i].remark === "G" && allEntries[i].isWhite === true) {
          newScore.white++;
        } else if (
          allEntries[i].remark === "G" &&
          allEntries[i].isWhite === false
        ) {
          newScore.dark++;
        }
      }
    }
    return newScore;
  };

  const getOtTwoQuaterScore = (score) => {
    let newScore = { ...score };
    for (let i = 0; i < allEntries.length; i++) {
      if (allEntries[i].period === "OT2") {
        if (allEntries[i].remark === "G" && allEntries[i].isWhite === true) {
          newScore.white++;
        } else if (
          allEntries[i].remark === "G" &&
          allEntries[i].isWhite === false
        ) {
          newScore.dark++;
        }
      }
    }
    return newScore;
  };
  scoreOne = getFirstQuaterScore(scoreOne);
  scoreTwo = getSecondQuaterScore(scoreTwo);
  scoreThree = getThirdQuaterScore(scoreThree);
  scoreFour = getFourthQuaterScore(scoreFour);
  scoreOt = getOtQuaterScore(scoreOt);
  scoreOtTwo = getOtTwoQuaterScore(scoreOtTwo);
  let finalWhite =
    scoreOne.white +
    scoreTwo.white +
    scoreThree.white +
    scoreFour.white +
    scoreOt.white +
    scoreOtTwo.white;
  let finalDark =
    scoreOne.dark +
    scoreTwo.dark +
    scoreThree.dark +
    scoreFour.dark +
    scoreOt.dark +
    scoreOtTwo.dark;
  return (
    <div>
      <table className="ui celled table">
        <tr>
          <th>Results</th>
          <th>W</th>
          <th>D</th>
        </tr>
        <tr>
          <td>1st</td>
          <td>{scoreOne.white}</td>
          <td>{scoreOne.dark}</td>
        </tr>
        <tr>
          <td>2nd</td>
          <td>{scoreTwo.white}</td>
          <td>{scoreTwo.dark}</td>
        </tr>
        <tr>
          <td>3rd</td>
          <td>{scoreThree.white}</td>
          <td>{scoreThree.dark}</td>
        </tr>
        <tr>
          <td>4th</td>
          <td>{scoreFour.white}</td>
          <td>{scoreFour.dark}</td>
        </tr>
        <tr>
          <td>OT</td>
          <td>{scoreOt.white}</td>
          <td>{scoreOt.dark}</td>
        </tr>
        <tr>
          <td>OT2</td>
          <td>{scoreOtTwo.white}</td>
          <td>{scoreOtTwo.dark}</td>
        </tr>
        <tr>
          <td>Final</td>
          <td>{finalWhite}</td>
          <td>{finalDark}</td>
        </tr>
      </table>
    </div>
  );
};

export default Results;
