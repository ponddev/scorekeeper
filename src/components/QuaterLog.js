import react, { useState } from "react";
import { useSelector } from "react-redux";
import QuaterLogEntry from "./QuaterLogEntry";

const QuaterLog = (props) => {
  const getQuaterEntries = (allEntries) => {
    let array = [];
    for (let i = 0; i < allEntries.length; i++) {
      if (allEntries[i].period === props.period) {
        array.push(allEntries[i]);
      }
    }
    return array;
  };
  const allEntries = useSelector((state) => state.logEntries);
  let quater = getQuaterEntries(allEntries);

  let score = { white: 0, dark: 0 };
  const calcScore = (score, entry) => {
    let newScore = { ...score };

    if (entry.remark === "G") {
      if (entry.isWhite) {
        newScore.white++;
      } else {
        newScore.dark++;
      }
    }
    return newScore;
  };

  const renderQuaterLogEntry = (entry, index) => {
    score = calcScore(score, entry);
    return (
      <QuaterLogEntry
        time={`${entry.minute}:${String(entry.second).padStart(2, "0")}`}
        cap={entry.cap}
        remark={entry.remark}
        score={score}
        isWhite={entry.isWhite}
      />
    );
  };

  return (
    <table className="ui celled table">
      <tr>
        <th>Time</th>
        <th>W</th>
        <th>D</th>
        <th>Remarks</th>
        <th>W-D</th>
      </tr>
      {quater.map((entry, index) => renderQuaterLogEntry(entry, index))}
    </table>
  );
};

export default QuaterLog;
