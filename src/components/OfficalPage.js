import React, { useState } from "react";
import OfficalTeamStats from "./OfficalTeamStats";
import TimeoutStats from "./TimeoutStats";
import QuaterLog from "./QuaterLog";
import Results from "./Results";
import OfficialGameLog from "./OfficialGameLog";
import { useSelector } from "react-redux";

const OfficalPage = () => {
  const game = useSelector((state) => state.setUp);

  const numPlayers = Number(game.numPlayers);

  const getPlayerList = () => {
    let playerList = [];
    for (let i = 1; i <= numPlayers; i++) {
      playerList.push({ cap: `${i}` });
      if (i === 1) {
        playerList.push({ cap: "1a" });
      }
    }
    return playerList;
  };

  return (
    <div className="offical">
      <div className="ui two column  grid">
        <div className="column">
          <OfficalTeamStats
            className="offical"
            playerList={getPlayerList()}
            isWhite={true}
            team={game.white}
          />
        </div>
        <div className="column">
          <OfficalTeamStats
            className="offical"
            playerList={getPlayerList()}
            isWhite={false}
            team={game.dark}
          />
        </div>
      </div>
      <div className="ui two column grid">
        <div className="column">
          <TimeoutStats isWhite={true} />
        </div>
        <div className="column">
          <TimeoutStats isWhite={false} />
        </div>
      </div>
      <OfficialGameLog />
    </div>
  );
};

export default OfficalPage;
