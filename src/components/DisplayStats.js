import React from "react";
import TeamStats from "./TeamStats";
import GameLog from "./GameLog";
import { useSelector } from "react-redux";

const DisplayStats = () => {
  const game = useSelector((state) => state.setUp);
  return (
    <div className="ui three column centered grid">
      <div className="column">
        <TeamStats team={game.white} isWhite={true} />
      </div>
      <div className="column">
        <TeamStats team={game.dark} isWhite={false} />
      </div>
      <div className="column">
        <GameLog />
      </div>
    </div>
  );
};

export default DisplayStats;
