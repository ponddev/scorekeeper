import { createStore } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import RootReducer from "../reducer/RootReducer";
import { v4 as uuidv4, v4 } from "uuid";

const gameOneId = v4();
const allGames = [{ title: "Milford vs Mason 4/20/21 ", uuid: gameOneId }];
export const saveState = (key, state) => {
  try {
    const serialState = JSON.stringify(state);
    localStorage.setItem(key, serialState);
  } catch (err) {
    console.log(err);
  }
};

export const loadState = () => {
  try {
    const serialState = localStorage.getItem("All-Games");
    if (serialState === null) {
      return undefined;
    }
    return JSON.parse(serialState);
  } catch (err) {
    return undefined;
  }
};

const persistedState = loadState();

const Store = createStore(RootReducer, persistedState, composeWithDevTools());

Store.subscribe(() => saveState("All-Games", allGames));

Store.subscribe(() =>
  saveState(gameOneId, {
    Store: Store.getState(),
  })
);

export default Store;
