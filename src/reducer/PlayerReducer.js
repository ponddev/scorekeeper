const intialState = [
  { cap: "1", isWhite: true },
  { cap: "1a", isWhite: true },
  { cap: "1", isWhite: false },
  { cap: "1a", isWhite: false },
];

export default function reducer(currentState = intialState, action) {
  switch (action.type) {
    case "ADD_PLAYER":
      let newArray = [...currentState];
      newArray.push({
        cap: action.payload.cap,
        isWhite: action.payload.isWhite,
      });
      return newArray;
    default:
      return intialState;
  }
}
