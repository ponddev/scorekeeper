const intialState = {
  white: "Milford",
  dark: "Mason",
  numTo: "",
  numOt: "",
  lengthPeriod: "",
  numPlayers: "25",
};

export default function reducer(currentState = intialState, action) {
  switch (action.type) {
    case "CREATE_GAME":
      let newGame = currentState;
      newGame = {
        white: action.payload.white,
        dark: action.payload.dark,
        numTo: action.payload.numTo,
        numOt: action.payload.numOt,
        lengthPeriod: action.payload.lengthPeriod,
        numPlayers: action.payload.numPlayers,
      };
      return newGame;
    default:
      return currentState;
  }
}
