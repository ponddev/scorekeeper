import { combineReducers } from "redux";
import Reducer from "./Reducer";
import PeriodReducer from "./PeriodReducer";
import TimeReducer from "./TimeReducer";
import PlayerReducer from "./PlayerReducer";
import StartReducer from "./StartReducer";

const RootReducer = combineReducers({
  logEntries: Reducer,
  period: PeriodReducer,
  time: TimeReducer,
  playerList: PlayerReducer,
  setUp: StartReducer,
});

export default RootReducer;
