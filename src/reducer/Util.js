export const getIndex = (array, item) => {
  let index = 0;
  for (let i = 0; i < array.length; i++) {
    if (item.period > array[i].period) {
      index = i + 1;
    } else if (
      item.period === array[i].period &&
      item.minute < array[i].minute
    ) {
      index = i + 1;
    } else if (
      item.period === array[i].period &&
      item.minute === array[i].minute &&
      item.second < array[i].second
    ) {
      index = i + 1;
    }
  }
  return index;
};
