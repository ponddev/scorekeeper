const intialState = { minute: "8", tenSecond: "0", oneSecond: "0" };

export default function reducer(currentState = intialState, action) {
  switch (action.type) {
    case "CHANGE_TIME":
      let newTime = currentState;
      newTime = {
        minute: action.payload.minute,
        tenSecond: action.payload.tenSecond,
        oneSecond: action.payload.oneSecond,
      };
      return newTime;
    default:
      return currentState;
  }
}
