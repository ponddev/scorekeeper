import { getIndex } from "./Util";
const intialState = [];

export default function reducer(currentState = intialState, action) {
  switch (action.type) {
    case "ADD_ENTRY":
      let newArray = [...currentState];
      let index = getIndex(newArray, {
        period: action.payload.period,
        minute: action.payload.minute,
        second: action.payload.second,
        cap: action.payload.cap,
        remark: action.payload.remark,
        isWhite: action.payload.isWhite,
      });
      if (index >= newArray.length) {
        newArray.push({
          period: action.payload.period,
          minute: action.payload.minute,
          second: action.payload.second,
          cap: action.payload.cap,
          remark: action.payload.remark,
          isWhite: action.payload.isWhite,
        });
      } else {
        newArray.splice(index, 0, {
          period: action.payload.period,
          minute: action.payload.minute,
          second: action.payload.second,
          cap: action.payload.cap,
          remark: action.payload.remark,
          isWhite: action.payload.isWhite,
        });
      }
      return newArray; // [
    //  ...currentState,
    //  {
    //    period: action.payload.period,
    //    minute: action.payload.minute,
    //    second: action.payload.second,
    //    cap: action.payload.cap,
    //    remark: action.payload.remark,
    //    isWhite: action.payload.isWhite,
    //   },
    // ];

    case "ADD_ENTRIES":
      let fakeArray = [...action.payload.array];
      //fakeArray.concat(action.payload);

      return fakeArray;

    case "DELETE":
      let deleteArray = [...currentState];
      deleteArray.splice(action.payload, 1);

      return deleteArray;
    default:
      return currentState;
  }
}
