const intialState = "";

export default function reducer(currentState = intialState, action) {
  switch (action.type) {
    case "CHANGE_PERIOD":
      let newPeriod = currentState;
      newPeriod = action.payload.period;
      return newPeriod;
    default:
      return currentState;
  }
}
