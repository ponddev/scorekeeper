import React from "react";
import ReactDOM from "react-dom";
import App from "./components/App";

import { createStore } from "redux";
import { Provider } from "react-redux";
import Store from "./components/Store";

ReactDOM.render(
  <Provider store={Store}>
    <App />
  </Provider>,

  document.querySelector("#root")
);
